# Exemple d'utilisation du script

## Installation des pré-requis

Les packages requit à l'exécution du script sont gérés dans le fichier requirements.txt  
**/!\\** Le script doit être exécuté en "_python3_"
Placez-vous à la racine du projet

```
pip install -r requirements.txt --user
```
Il se peut que vous deviez utiliser la commande **"pip3",** au lieu de **"pip"**

## Extraction des données

### Les fichiers JSON
Les fichiers sont situés dans le dossier "fichiers-json". L'exécution du script "extract-op-data.py" permet d'extraire les fichiers JSON vers un format exploitable

```
./extract-op-data.py convert csv -p ./exemples/fichiers-json/base2-motions/ -o ./fichiers-convert/base2-motions -e false
```
Dans cet exemple, vous obtenez un fichier "video-1.csv", situé dans le dossier fichiers-convert

L'option -e permet de concerver les points, qui n'ont pas été détectés. En ajoutant **-e false**, on va interpoler les points manquants.

## Affichage des données

### Calcul des distances

Calcul des mouvemnts réalisés par le bras droit (voir documentation)
```
./motion-graph.py graph -f ./exemples/fichiers-convert/base2-motions/base2-motions_pose_keypoints_2d_20180523-132558.csv -i arm_r
```

### Calcul APC
```
./motion-graph.py acp -f ./exemples/fichiers-convert/base2-motions/base2-motions_pose_keypoints_2d_20180523-132558.csv -i arm_r
```

### Décomposition des mouvements
```
./motion-graph.py motions -f ./exemples/fichiers-convert/base2-motions/base2-motions_pose_keypoints_2d_20180523-132558.csv -i arm_r
```