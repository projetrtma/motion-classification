# motion-classification

## Conversion des données de Openpose

Commande "extract-op-data.py"

_Exemple_ :
```shell
$ ./extract-op-data.py convert csv -p ./chemin/nom-du-dossier/ -o ./chemin/nom-du-dossier/nom-du-fichier -e false
```

_Command_ :  
**convert** : Conversion des fichiers json, d'Openpose  

_type_ :  
**csv** : Conversion en csv

_arguments_ :  
**-p** : Chemin contenant les fichier json, extrait d'Openpose  
**-o** : Chemin de destination et le nom du fichier, pas d'extention  
**-e** : Garder les zones vides, False permet d'estimer les valeurs vides

#### Fichier Help

```
Manipulate Openpose Data - Convert all JSON files Openpose extraction
 
Usage:
  extract-op-data.py convert <type> [--path <path>] [--out <out>] [--empty <val>]
  extract-op-data.py -h
  extract-op-data.py --version
   
Examples:

Arguments:

Options:
  -h, --help
  -p <path>, --path <path>              Json files directory
  -o <out>, --out <out>                 Output path and file name
  -d <detect>, --detect <detect>        Element's type to detect
  -e <val>, --empty <val>               keep empty values (1, true | 0, false) [default: 1]
  --version
```

## Analyse des données formatées

Commande "motion-graph.py"

_Exemple_ :
```shell
$ ./motion-graph.py graph stats -f ./chemin/nom-du-dossier/nom-du-fichier.csv -i arm_r
```

_Command_ :  
**graph** : Affichage des mouvements extrait de données, du barycentre et de la séparation des mouvements  
**acp** : Calcul de l'ACP et des différents mouvements 
**motions** : Affichage des mouvements détectés 

_arguments_ :  
**-f** : Chemin complet du fichier csv  
**-i** : Membre(s) du corps à inclure (elements séparés par des virgules)  
**-e** : Membre(s) du corps à inclure (elements séparés par des virgules) 

_Identification des membres_
* arm_r     : bras droit
* arm_l     : bras gauche
* leg_r     : jambe droite
* leg_l     : jambe gauche
* head      : tête

```
Motion graph - Generate graph from CSV file with the body points (COCO version)
 
Usage:
  motion-graph.py [graph] [acp] [motions] --file <path> [--include <ele> | --exclude <ele>]
  motion-graph.py -h
  motion-graph.py --version
   
Examples:
  motion-graph.py graph --file ~/Documents/body.csv --include arm_l,arm_r
  motion-graph.py graph --file ~/Documents/body.csv --exclude head,leg_r,leg_l

Options:
  -h, --help
  -f <path>, --file <path>              CSV file name and directory
  -i <ele>, --include <ele>             Detected elements
  -e <ele>, --exclude <ele>             Excluded elements
  -v, --version
```