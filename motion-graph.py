#! /usr/bin/python3

"""
Motion graph - Generate graph from CSV file with the body points (COCO version)
 
Usage:
  motion-graph.py [graph] [acp] [motions] --file <path> [--include <ele> | --exclude <ele>]
  motion-graph.py -h
  motion-graph.py --version
   
Examples:
  motion-graph.py graph --file ~/Documents/body.csv --include arm_l,arm_r
  motion-graph.py graph --file ~/Documents/body.csv --exclude head,leg_r,leg_l

Options:
  -h, --help
  -f <path>, --file <path>              CSV file name and directory
  -i <ele>, --include <ele>             Detected elements
  -e <ele>, --exclude <ele>             Excluded elements
  -v, --version
 
"""
import csv
import sys
import math
import matplotlib.pyplot as plt
import numpy as np

from collections import defaultdict
from collections import OrderedDict

from docopt import docopt

from sklearn import datasets
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

nb_frame_out    = 4
threshold       = 6

def main():

    elements = defaultdict()
    # Position of each body parts
    elements['position'] = {
        'arm_r'   : ([1,2,3,4]),
        'arm_l'   : ([1,5,6,7]),
        'leg_r'   : ([1,8,9,10]),
        'leg_l'   : ([1,11,12,13]), 
        'head'    : ([1,0,14,15,16,17])
    }
    # Weigth of each position
    elements['weigth'] = {
        'arm_r'   : ([1,2,4,8]),
        'arm_l'   : ([1,2,4,8]),
        'leg_r'   : ([1,2,4,8]),
        'leg_l'   : ([1,2,4,8]),
        'head'    : ([1,2,4,4,8,8])
    }

    
    opt = docopt(__doc__, sys.argv[1:], version='0.01')
    
    handle          = open(opt['--file'], 'r')
    points          = csv.reader(handle)
    elements        = define_new_points(elements, excluded_points(points))

    # Check the items to keep
    elements        = define_members(elements, opt)
    
    collected_data   = defaultdict()
    collected_data['centroid']   = {
        'cent_x': {item:[] for item in elements['position']},
        'cent_y': {item:[] for item in elements['position']},
        'distance': {item:[] for item in elements['position']}
    }
    collected_data['orientation']   = {
        'x': {item:[] for item in elements['position']},
        'y': {item:[] for item in elements['position']}
    }
    collected_data['position']   = {
        'x': {item:[] for item in elements['position']},
        'y': {item:[] for item in elements['position']}
    }

    tmp_cent_x      = {item:0 for item in elements['position']}
    tmp_cent_y      = {item:0 for item in elements['position']}
    diff_motion     = {item:[] for item in elements['position']}
    dist_centroid   = ""
    
    handle.seek(0)
    for r,row in enumerate(points):
        # Reduce the interation number
        if r % nb_frame_out == 0:
            v_x = []
            v_y = []
            for c,col in enumerate(row):
                col = float(col)
                if c % 2 == 0:
                    v_x.append(col)
                else:
                    v_y.append(-col)
            
            for item in elements['position']:
                # print(collected_data)
                
                vect_x      = [v_x[idx] for idx in elements['position'][item]]
                vect_y      = [v_y[idx] for idx in elements['position'][item]]
                collected_data['position']['x'][item].append(vect_x)
                collected_data['position']['y'][item].append(vect_y)

                cent_x      = centroid(vect_x, elements['weigth'][item])
                cent_y      = centroid(vect_y, elements['weigth'][item])
                collected_data['centroid']['cent_x'][item].append(cent_x)
                collected_data['centroid']['cent_y'][item].append(cent_y)

                orient_x    = orientation(cent_x, tmp_cent_x[item])
                orient_y    = orientation(cent_y, tmp_cent_y[item])
                m_x         = -(orient_x[1] - orient_x[0])
                m_y         = orient_y[1] - orient_y[0]
                collected_data['orientation']['x'][item].append(m_x)
                collected_data['orientation']['y'][item].append(m_y)
                
                dist_centroid   = np.sqrt((orient_x[1] - orient_x[0])**2 + (orient_y[1] - orient_y[0])**2)
                if (dist_centroid != "" ):
                    collected_data['centroid']['distance'][item].append(dist_centroid)

                if -threshold < m_x < threshold and -threshold < m_y < threshold:
                    diff_motion[item].append(r // nb_frame_out)

                tmp_cent_x[item] = cent_x
                tmp_cent_y[item] = cent_y


    

    diff_motion = check_diff_motion(diff_motion, r)

    if opt['acp'] and opt['motions']:
        motions = get_motions(elements, collected_data, diff_motion)
        draw_acp_motions(motions, 5, 10, graph=["plot","ratio"])
    elif opt['graph']:
        draw_graph(elements, collected_data, threshold, diff_motion)
    elif opt['acp']:
        motions = get_motions(elements, collected_data, diff_motion)
        analysis_acp(motions)
    elif opt['motions']:
        motions = get_motions(elements, collected_data, diff_motion)
        draw_motions(motions)
    handle.close()

def define_members(elements, opt):
    """
    Create the final list to define the activate body parts
    --include : Keep only there values
    --exclude : Remove defined values
    """
    if opt['--include'] is None and opt['--exclude'] is None:
        return elements
    elif opt['--include'] is not None:
        inc_ele = opt['--include'].split(',')
        elements['position'] = {item:elements['position'][item] for item in inc_ele}
        return elements
    elif opt['--exclude'] is not None:
        exc_ele = opt['--exclude'].split(',')
        for item in exc_ele:
            del elements['position'][item]
        return elements

def excluded_points(points):
    """
    Define the exclude points list, where a column has an empty value
    """
    points   = list(points)
    num_cols = int(len(points[0]) / 2)
    exclude  = []

    for i in range(num_cols):
        for frame in points:
            if frame[i*2] == "0":
                # Add the index containing empty value
                exclude.append(i)
                break

    return exclude

def define_new_points(elements, exclude):
    """
    Remove all columns contain empty values
    """
    for item in elements['position']:
        [elements['position'][item].remove(val) for val in exclude if val in elements['position'][item]]
        while len(elements['position'][item]) != len(elements['weigth'][item]):
            elements['weigth'][item].pop()
    return elements

def centroid(elements, weigth):
    """
    Get the elements centroid
    """
    total_weigth = sum(weigth)
    np_weigth    = np.array(weigth)
    np_elements  = np.array(elements)

    return 1 / total_weigth * sum(np_elements * np_weigth)

def orientation(current_cent, prev_cent):
    """
    Define the orientation, depends on the centroid
    """
    if prev_cent != 0:
        orient_size = (current_cent - prev_cent) / 2
        return (current_cent - orient_size, current_cent + orient_size)
    return (current_cent, current_cent)

def get_motions(elements, collected_data, diff_motion):
    """

    """
    motions     = {item:[] for item in elements['position']}
    tmp_i       = 0
    
    for item in OrderedDict(sorted(elements['position'].items(), key=lambda t: t[0])):
        
        
        for i,pos in enumerate(diff_motion[item]):

            tmp_motion  = []
            if i > 0:
                is_motion = diff_motion[item][i] - diff_motion[item][tmp_i]
                if is_motion > 1:
                    for k in range(is_motion):
                        current_motion = []
                        pos_in_motion = diff_motion[item][tmp_i] + k
                        len_pos       = len(collected_data['position']['x'][item][pos_in_motion])
                        for l in range(len_pos):
                            current_motion.append(collected_data['position']['x'][item][pos_in_motion][l])
                            current_motion.append(collected_data['position']['y'][item][pos_in_motion][l])
                        tmp_motion.append(current_motion)

                    motions[item].append(tmp_motion)
            tmp_i = i
    return motions

def analysis_acp(motions):
    """

    """
    pca = PCA()

    for item in motions:
        for i in range(len(motions[item])):
            arm_r   = pca.fit(motions[item][i]).transform(motions[item][i])
            # S       = arm_r / np.std(arm_r, axis=0)

            plt.figure(i)
            # plt.scatter(arm_r[:, 0], arm_r[:, 1], s=2, marker='o', zorder=10, color='steelblue', alpha=1)
            for k in range(4 - 1):
                plt.plot(arm_r[:, k+1], arm_r[:, 0])
            plt.show()
    return ""

def draw_motions(motions):
    
    plt.figure("acp and motions")
    for item in motions:
        fig, ax = plt.subplots(4, 5)
        nb_motions = len(motions[item])
        

        for i,motion in enumerate(motions[item]):
            path_x = []
            path_y = []
            row = (i//5)%5
            col = i%5
            
            for pos in motion:
                
                ax[row][col].plot(
                    [pos[0], pos[2], pos[4], pos[6]], 
                    [pos[1], pos[3], pos[5], pos[7]], 
                    color="blue",
                    linewidth=0.5)
                # get the path
                path_x.append(pos[6])
                path_y.append(pos[7])

            ax[row][col].plot(path_x, path_y, color="red", linewidth=0.5)
            ax[row][col].arrow(path_x[0],path_y[0], path_x[1] - path_x[0],path_y[1] - path_y[0], head_width=3, head_length=2, fc='r', ec='r', linewidth=5)

        fig.tight_layout()
        plt.show()

def draw_acp_motions(motions, nb_cols, nb_elements_max="all", **type):

    pca         = PCA()
    color       = ["red", "blue", "green", "pink", "yellow", "purple"]
    item        = "arm_r"
    nb_graph    = len(type['graph'])

    nb_motions  = len(motions[item])
    if isinstance(nb_elements_max, int):
        nb_motions = nb_elements_max

    fig, ax = plt.subplots(math.ceil(nb_motions / nb_cols) * (nb_graph+1), nb_cols)
    for i,motion in enumerate(motions[item]):
        
        motions_t = np.transpose(motions[item][i])
        # motions_t = motions[item][i]
        path_x = []
        path_y = []
        row = ((i//nb_cols) % nb_cols) * (nb_graph+1)
        col = i%nb_cols

        arm_r   = pca.fit_transform(motions_t)
        # arm_r   = StandardScaler().fit_transform(motions_t)

        if "plot" in type['graph']:
            for p in range(2):
                # ax[row][col].plot(arm_r[:, 0], arm_r[:, p+1], color=color[p], linewidth=0.5)
                ax[row][col].plot(arm_r[:, 0], arm_r[:, p+1], "o", color=color[p], markersize=2)
        if "bar" in type['graph']:
            for b in range(2):
                ax[row+1][col].bar(range(len(arm_r[:, 0])), arm_r[:, b+1], color=color[b], alpha=0.5)
                # ax[row+1][col].axis([0,8,-500,500])
        if "ratio" in type['graph']:
            ax[row+1][col].plot(np.cumsum(pca.explained_variance_ratio_))
        
        for pos in motion:
            ax[row+nb_graph][col].plot(
                [pos[0], pos[2], pos[4], pos[6]], 
                [pos[1], pos[3], pos[5], pos[7]], 
                color="blue",
                linewidth=0.5)
            # get the path
            path_x.append(pos[6])
            path_y.append(pos[7])

        ax[row+nb_graph][col].plot(path_x, path_y, color="red", linewidth=0.5)
        ax[row+nb_graph][col].arrow(path_x[0],path_y[0], path_x[1] - path_x[0],path_y[1] - path_y[0], head_width=3, head_length=2, fc='r', ec='r', linewidth=4)

        if nb_elements_max != "all" and i == nb_elements_max - 1:
            break
    # fig.tight_layout()
    plt.show()

def draw_graph(elements, collected_data, threshold, diff_motion):
    """
    
    """
    size_pos        = len(elements['position'])
    size_sub_plot   = 1 + 2 * size_pos

    plt.figure()
    plt.subplot(size_sub_plot,1,1)
    for item in elements['position']:
        for i in range(len(collected_data['position']['x'][item])):
            pt_motion,   = plt.plot(
                collected_data['position']['x'][item][i], 
                collected_data['position']['y'][item][i], 
                color="blue", 
                linewidth=0.5)
        
            pt_orient,   = plt.plot(
                collected_data['orientation']['x'][item][i], 
                collected_data['orientation']['y'][item][i], 
                color="red", 
                linewidth=0.5)

        pt_centroid, = plt.plot(
            collected_data['centroid']['cent_x'][item], 
            collected_data['centroid']['cent_y'][item], 
            "ro")

        plt.legend([pt_motion, pt_orient, pt_centroid], ['motions', 'direction', 'centroid'])
        plt.xlabel('X position')
        plt.ylabel('Y position')
        plt.title('Human motions')

    k = 1
    for item in elements['position']:

        plt.subplot(size_sub_plot,1,k+1)
        size_dist       = len(collected_data['centroid']['distance'][item])
        histx           = np.arange(size_dist)
        plt.xticks(np.arange(0,size_dist,20))
        plt.bar(histx, collected_data['centroid']['distance'][item])
        plt.xlabel('Frame')
        plt.ylabel('Distance')
        plt.title('"' + item + '", Distance between two points')

        plt.subplot(size_sub_plot,1,k+2)
        diff   = plt.bar(diff_motion[item], [2*threshold] * len(diff_motion[item]), bottom=-threshold, color='r', width=0.4)
        mouv_x = plt.bar(histx, collected_data['orientation']['x'][item])
        mouv_y = plt.bar(histx, collected_data['orientation']['y'][item], alpha=0.6)
        plt.legend([mouv_x, mouv_y, diff], ['West - Est', 'North - South', 'motions division'])
        plt.xlabel('Frame')
        plt.ylabel('Distance')
        plt.title('Representation of displacements')

        k += 2
    plt.show()

def check_diff_motion(diff_motion, total_frame):
    """

    """
    for item in diff_motion:
        if diff_motion[item][-1] < total_frame:
            diff_motion[item].append(total_frame // nb_frame_out)
    return diff_motion

if __name__ == "__main__":
    main()
